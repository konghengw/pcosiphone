<?php
    
    require("Conn.php");
    require("MySQLDao.php");
    $patient_id = $_POST['patient_id'];
    $password = $_POST['password'];
    $returnValue = array();
    
    if(empty($patient_id) || empty($password))
    {
        $returnValue["status"] = "error";
        $returnValue["message"] = "Missing required field";
        echo json_encode($returnValue);
        return;
    }
    
    $dao = new MySQLDao();
    $dao->openConnection();
    $userDetails = $dao->getUserDetailsWithPassword($patient_id,$password);
    
    if(!empty($userDetails))
    {
        $returnValue["status"] = "Success";
        $returnValue["message"] = "User is registered";
        echo json_encode($returnValue);
    } else {
        
        $returnValue["status"] = "error";
        $returnValue["message"] = "User is not found";
        echo json_encode($returnValue);
    }
    
    $dao->closeConnection();
    
    ?>
