<?php
    
    
    require("Conn.php");
    require("MySQLDao.php");
    $patient_id = $_POST['username'];
    $password = $_POST['password'];
    $returnValue = array();
    
    if(empty($patient_id) || empty($password))
    {
        $returnValue["status"] = "error";
        $returnValue["message"] = "Missing required field";
        echo json_encode($returnValue);
        return;
    }
    
    $dao = new MySQLDao();
    $dao->openConnection();
    $userDetails = $dao->getUserDetails($patient_id);
    
    if(!empty($userDetails))
    {
        $returnValue["status"] = "error";
        $returnValue["message"] = "User already exists";
        echo json_encode($returnValue);
        return;
    }
    
    $result = $dao->registerUser($patient_id,$password);
    
    if($result)
    {
        $returnValue["status"] = "Success";
        $returnValue["message"] = "User is registered";
        echo json_encode($returnValue);
        return;
    }
    
    $dao->closeConnection();
    
    ?>
