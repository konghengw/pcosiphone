<?php
    class MySQLDao {
        var $dbhost = null;
        var $dbuser = null;
        var $dbpass = null;
        var $conn = null;
        var $dbname = null;
        var $result = null;
        
        function __construct() {
            $this->dbhost = Conn::$dbhost;
            $this->dbuser = Conn::$dbuser;
            $this->dbpass = Conn::$dbpass;
            $this->dbname = Conn::$dbname;
        }
        
        public function openConnection() {
            $this->conn = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);
            if (mysqli_connect_errno())
                echo new Exception("Could not establish connection with database");
        }
        
        public function getConnection() {
            return $this->conn;
        }
        
        public function closeConnection() {
            if ($this->conn != null)
                $this->conn->close();
        }
        
        public function getUserDetails($patient_id)
        {
            $returnValue = array();
            $sql = "select * from user where patient_id='" . $patient_id . "'";
            
            $result = $this->conn->query($sql);
            if ($result != null && (mysqli_num_rows($result) >= 1)) {
                $row = $result->fetch_array(MYSQLI_ASSOC);
                if (!empty($row)) {
                    $returnValue = $row;
                }
            }
            return $returnValue;
        }
        
        public function getUserDetailsWithPassword($patient_id, $userPassword)
        {
            $returnValue = array();
            $sql = "select patient_id from user where patient_id='" . $patient_id . "' and password='" .$userPassword . "'";
            
            $result = $this->conn->query($sql);
            if ($result != null && (mysqli_num_rows($result) >= 1)) {
                $row = $result->fetch_array(MYSQLI_ASSOC);
                if (!empty($row)) {
                    $returnValue = $row;
                }
            }
            return $returnValue;
        }
        
        public function registerUser($patient_id, $password)
        {
            $sql = "insert into user set patient_id=?, password=?";
            $statement = $this->conn->prepare($sql);
            
            if (!$statement)
                throw new Exception($statement->error);
            
            $statement->bind_param("ss", $patient_id, $password);
            $returnValue = $statement->execute();
            
            return $returnValue;
        }
        
    }
    ?>
