//
//  HADSAnswer.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/5/3.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import Foundation

class HADSAnswer {
    
    let scoreThreeState: Bool
    let scoreTwoState : Bool
    let scoreOneState : Bool
    let scoreZeroState: Bool
    
    init(threeState : Bool, twoState : Bool, oneState : Bool, zeroState: Bool) {
        self.scoreThreeState = threeState
        self.scoreTwoState = twoState
        self.scoreOneState = oneState
        self.scoreZeroState = zeroState
    }
    
}
