//
//  SubmitTableViewCell.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/4/30.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit

class SubmitTableViewCell: UITableViewCell {

    @IBOutlet weak var submitButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
