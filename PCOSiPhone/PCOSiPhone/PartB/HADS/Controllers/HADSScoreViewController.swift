//
//  HADSScoreViewController.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/5/1.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit

class HADSScoreViewController: UIViewController {
    
    var depression: String = ""
    var anxiety: String = ""
    
    @IBOutlet weak var depressionScore: UILabel!
    @IBOutlet weak var anxietyScore: UILabel!
    @IBOutlet weak var homeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        depressionScore?.text = depression
        anxietyScore?.text = anxiety
        
        homeButton.layer.cornerRadius = 4
    }

}
