//
//  HADSViewController.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/4/28.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit

class HADSViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var insertData: [Any] = [0,0,"","","","","","","","","","","","","","",""]
    var answerState: [Int: [Bool]] = [:]
    var list = [HADSQuestion]()
    var depressionScore: Int = 0
    var anxietyScore: Int = 0
    var cellScore: [Int] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    var submitEnable: Bool = false
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        loadHADS()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    // MARK: - Table view data source
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "HADSScoreVC"
        {
            depressionScore = cellScore[1]+cellScore[2]+cellScore[5]+cellScore[6]+cellScore[9]+cellScore[10]+cellScore[13]
            anxietyScore = cellScore[0]+cellScore[3]+cellScore[4]+cellScore[7]+cellScore[8]+cellScore[11]+cellScore[12]
            let vc = segue.destination as? HADSScoreViewController
            vc?.depression = String(depressionScore)
            vc?.anxiety = String(anxietyScore)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        if indexPath.row < 14 {
            let cellIdentifier = "HADSTableViewCell"
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? HADSTableViewCell  else {
                fatalError("The dequeued cell is not an instance of HADSTableViewCell.")
            }
        
            let quest = list[indexPath.row]
        
            cell.question.text = quest.questionText
            cell.optionOne.text = quest.answer[0]
            cell.optionTwo.text = quest.answer[1]
            cell.optionThree.text = quest.answer[2]
            cell.optionFour.text = quest.answer[3]
            
            cell.scoreThree.tag = indexPath.row
            cell.scoreTwo.tag = indexPath.row
            cell.scoreOne.tag = indexPath.row
            cell.scoreZero.tag = indexPath.row
            
            let radioButtons = answerState[indexPath.row]!
            
            // Get the state of each radio button
            cell.scoreThree.isSelected = radioButtons[0]
            cell.scoreTwo.isSelected = radioButtons[1]
            cell.scoreOne.isSelected = radioButtons[2]
            cell.scoreZero.isSelected = radioButtons[3]
            
            cell.scoreThree.addTarget(self, action: #selector(threeSelected(_:)), for: .touchUpInside)
            cell.scoreTwo.addTarget(self, action: #selector(twoSelected(_:)), for: .touchUpInside)
            cell.scoreOne.addTarget(self, action: #selector(oneSelected(_:)), for: .touchUpInside)
            cell.scoreZero.addTarget(self, action: #selector(zeroSelected(_:)), for: .touchUpInside)
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubmitTableViewCell") as! SubmitTableViewCell
            // Get the submit button state
            if submitEnable == false {
                cell.submitButton.isEnabled = false
                cell.submitButton.alpha = 0.5
            } else {
                cell.submitButton.isEnabled = true
                cell.submitButton.alpha = 1
            }
            cell.submitButton.addTarget(self, action: #selector(submit(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < 14 {
            return 331;
        }
        else{
            return 84; //a default size if the cell index path is anything other than the 1st or second row.
        }
    }
    
    @objc func submit(_ sender: UIButton){
        let userString = UserDefaults.standard.string(forKey: "username")  ?? ""
        
        // Update value of ensat and center
        insertData[1] = Int(userString.suffix(1))!
        insertData[2] = (userString.prefix(4))
        
        let url = URL(string: "http://localhost/store.php")!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let parameters: [String: Any] = [
            "qhads": insertData[0],
            "ensat": insertData[1],
            "center": insertData[2],
            "q1": insertData[3],
            "q2": insertData[4],
            "q3": insertData[5],
            "q4": insertData[6],
            "q5": insertData[7],
            "q6": insertData[8],
            "q7": insertData[9],
            "q8": insertData[10],
            "q9": insertData[11],
            "q10": insertData[12],
            "q11": insertData[13],
            "q12": insertData[14],
            "q13": insertData[15],
            "q14": insertData[16]
        ]
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {
                    // check for fundamental networking error
                    print("error", error ?? "Unknown error")
                    return
            }
            
            guard (200 ... 299) ~= response.statusCode else {
                // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(responseString)")
        }
        
        task.resume()
    }
    
    @objc func threeSelected(_ sender: UIButton){
        // Update score for the cell
        cellScore[sender.tag] = 3
        // Update which radio button is selected
        answerState.updateValue([true,false,false,false], forKey: sender.tag)
        // Update the data sent to API
        insertData[sender.tag+3] = "3"
        // Check if submit button needs to be enabled
        submitEnable = true
        for n in 0...13 {
            if answerState[n] == [false,false,false,false] {
                submitEnable = false
            }
        }
        self.tableView.reloadData()
    }
    
    @objc func twoSelected(_ sender: UIButton){
        cellScore[sender.tag] = 2
        answerState.updateValue([false,true,false,false], forKey: sender.tag)
        insertData[sender.tag+3] = "2"
        submitEnable = true
        for n in 0...13 {
            if answerState[n] == [false,false,false,false] {
                submitEnable = false
            }
        }
        self.tableView.reloadData()
    }
    
    @objc func oneSelected(_ sender: UIButton){
        cellScore[sender.tag] = 1
        answerState.updateValue([false,false,true,false], forKey: sender.tag)
        insertData[sender.tag+3] = "1"
        submitEnable = true
        for n in 0...13 {
            if answerState[n] == [false,false,false,false] {
                submitEnable = false
            }
        }
        self.tableView.reloadData()
    }
    
    @objc func zeroSelected(_ sender: UIButton){
        cellScore[sender.tag] = 0
        answerState.updateValue([false,false,false,true], forKey: sender.tag)
        insertData[sender.tag+3] = "0"
        submitEnable = true
        for n in 0...13 {
            if answerState[n] == [false,false,false,false] {
                submitEnable = false
            }
        }
        self.tableView.reloadData()
    }
    
    private func loadHADS() {
        // Put all questions into the list
        list.append(HADSQuestion(text: "I feel tense or 'wound up':",
                                 answer: ["Most of the time", "A lot of the time", "From time to time, occasionally", "Not at all"],
                                 whatFor: "A"))
        list.append(HADSQuestion(text: "I feel as if I am slowed down:",
                                 answer: ["Nearly all the time", "Very often", "Sometimes", "Not at all"],
                                 whatFor: "D"))
        list.append(HADSQuestion(text: "I still enjoy the things I used to enjoy:",
                                 answer: ["Hardly at all", "Only a little", "Not quite so much", "Definitely as much"],
                                 whatFor: "D"))
        list.append(HADSQuestion(text: "I get a sort of frightened feeling like 'butterflies' in the stomach:",
                                 answer: ["Very Often", "Quite Often", "Occasionally", "Not at all"],
                                 whatFor: "A"))
        list.append(HADSQuestion(text: "I get a sort of frightened feeling as if something awful is about to happen:",
                                 answer: ["Very definitely and quite badly", "Yes, but not too badly", "A little, but it doesn't worry me", "Not at all"],
                                 whatFor: "A"))
        list.append(HADSQuestion(text: "I have lost interest in my appearance:",
                                 answer: ["Definitely", "I don't take as much care as I should", "I may not take quite as much care", "I take just as much care as ever"],
                                 whatFor: "D"))
        list.append(HADSQuestion(text: "I can laugh and see the funny side of things:",
                                 answer: ["Not at all", "Definitely not so much now", "Not quite so much now", "As much as I always could"],
                                 whatFor: "D"))
        list.append(HADSQuestion(text: "I feel restless as I have to be on the move:",
                                 answer: ["Very much indeed", "Quite a lot", "Not very much", "Not at all"],
                                 whatFor: "A"))
        list.append(HADSQuestion(text: "Worrying thoughts go through my mind:",
                                 answer: ["A great deal of the time", "A lot of the time", "From time to time, but not too often", "Only occasionally"],
                                 whatFor: "A"))
        list.append(HADSQuestion(text: "I look forward with enjoyment to things:",
                                 answer: ["Hardly at all", "Definitely less than I used to", "Rather less than I used to", "As much as I ever did"],
                                 whatFor: "D"))
        list.append(HADSQuestion(text: "I feel cheerful:",
                                 answer: ["Not at all", "Not often", "Sometimes", "Most of the time"],
                                 whatFor: "D"))
        list.append(HADSQuestion(text: "I get sudden feelings of panic:",
                                 answer: ["Very often indeed", "Quite often", "Not very often", "Not at all"],
                                 whatFor: "A"))
        list.append(HADSQuestion(text: "I can sit at ease and feel relaxed:",
                                 answer: ["Not at all", "Not Often", "Usually", "Definitely"],
                                 whatFor: "A"))
        list.append(HADSQuestion(text: "I can enjoy a good book or radio or TV program:",
                                 answer: ["Very seldom", "Not often", "Sometimes", "Often"],
                                 whatFor: "D"))
        
        // Initialize all the radio button states
        for n in 0...13 {
            answerState.updateValue([false,false,false,false], forKey: n)
        }
        
    }

}
