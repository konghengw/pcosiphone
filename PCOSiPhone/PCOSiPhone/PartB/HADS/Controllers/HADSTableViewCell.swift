//
//  HADSTableViewCell.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/3/17.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit

class HADSTableViewCell: UITableViewCell {
    
    //MARK: Properties
    
    @IBOutlet weak var question: UITextView!
    @IBOutlet weak var optionOne: UITextField!
    @IBOutlet weak var optionTwo: UITextField!
    @IBOutlet weak var optionThree: UITextField!
    @IBOutlet weak var optionFour: UITextField!
    @IBOutlet weak var scoreThree: DLRadioButton!
    @IBOutlet weak var scoreTwo: DLRadioButton!
    @IBOutlet weak var scoreOne: DLRadioButton!
    @IBOutlet weak var scoreZero: DLRadioButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
