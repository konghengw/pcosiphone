//
//  PartAViewController.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/5/4.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit

class PartAViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var answerState: [Int: [Bool]] = [:]
    var list = [UIImage]()
    var cellHide: Bool = true
    var submitEnable: Bool = false
    var ageAnswered: Bool = false

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        loadImage()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row > 1 && indexPath.row < 11) {
            let cellIdentifier = "HairCTableViewCell"
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? HairCTableViewCell  else {
                fatalError("The dequeued cell is not an instance of HADSTableViewCell.")
            }
            
            let quest = list[indexPath.row-2]
            cell.hairImage.image = quest
            
            cell.buttonZero.tag = indexPath.row-2
            cell.buttonOne.tag = indexPath.row-2
            cell.buttonTwo.tag = indexPath.row-2
            cell.buttonThree.tag = indexPath.row-2
            cell.buttonFour.tag = indexPath.row-2
            
            // Get the radio button states
            let radioButtons = answerState[indexPath.row-2]!
            cell.buttonZero.isSelected = radioButtons[0]
            cell.buttonOne.isSelected = radioButtons[1]
            cell.buttonTwo.isSelected = radioButtons[2]
            cell.buttonThree.isSelected = radioButtons[3]
            cell.buttonFour.isSelected = radioButtons[4]
            
            cell.buttonZero.addTarget(self, action: #selector(zeroSelected(_:)), for: .touchUpInside)
            cell.buttonOne.addTarget(self, action: #selector(oneSelected(_:)), for: .touchUpInside)
            cell.buttonTwo.addTarget(self, action: #selector(twoSelected(_:)), for: .touchUpInside)
            cell.buttonThree.addTarget(self, action: #selector(threeSelected(_:)), for: .touchUpInside)
            cell.buttonFour.addTarget(self, action: #selector(fourSelected(_:)), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.row == 11 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PartASubmitTableViewCell") as! SubmitTableViewCell
            // Get the submit button state
            if submitEnable == false {
                cell.submitButton.isEnabled = false
                cell.submitButton.alpha = 0.5
            } else {
                cell.submitButton.isEnabled = true
                cell.submitButton.alpha = 1
            }
            return cell
        }
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HairBTableViewCell") as! HairBTableViewCell
            cell.answerField.addTarget(self, action: #selector(textFieldsIsNotEmpty),
                                        for: .editingChanged)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HairATableViewCell") as! HairATableViewCell
            
            cell.yesButton.addTarget(self, action: #selector(yesSelected(_:)), for: .touchUpInside)
            cell.noButton.addTarget(self, action: #selector(noSelected(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row > 1 && indexPath.row < 11 && cellHide == false) {
            return 133;
        }
        if (indexPath.row > 1 && indexPath.row < 11 && cellHide == true) {
            return 0;
        }
        else if indexPath.row == 11 {
            return 84;
        }
        else if (indexPath.row == 1 && cellHide == false) {
            return 140;
        }
        else if (indexPath.row == 1 && cellHide == true) {
            return 0;
        }
        else {
            return 111;
        }
    }
    
    @objc func yesSelected(_ sender: UIButton){
        cellHide = false
        // Check if submit button needs to be enabled
        submitEnable = true
        for n in 0...8 {
            if answerState[n] == [false,false,false,false,false] {
                submitEnable = false
            }
        }
        if ageAnswered == false {
            submitEnable = false
        }
        self.tableView.beginUpdates()
        // Reload the submit cell
        let selectedIndexPath = IndexPath(item:11 , section: 0)
        self.tableView.reloadRows(at: [selectedIndexPath], with: .none)
        self.tableView.endUpdates()
    }
    
    @objc func noSelected(_ sender: UIButton){
        cellHide = true
        submitEnable = true
        self.tableView.beginUpdates()
        let selectedIndexPath = IndexPath(item:11 , section: 0)
        self.tableView.reloadRows(at: [selectedIndexPath], with: .none)
        self.tableView.endUpdates()
    }
    
    @objc func zeroSelected(_ sender: UIButton){
        answerState.updateValue([true,false,false,false,false], forKey: sender.tag)
        // Check if submit button needs to be enabled
        submitEnable = true
        for n in 0...8 {
            if answerState[n] == [false,false,false,false,false] {
                submitEnable = false
            }
        }
        if ageAnswered == false {
            submitEnable = false
        }
        let selectedIndexPath = IndexPath(item:11 , section: 0)
        self.tableView.reloadRows(at: [selectedIndexPath], with: .none)
    }
    
    @objc func oneSelected(_ sender: UIButton){
        answerState.updateValue([false,true,false,false,false], forKey: sender.tag)
        // Check if submit button needs to be enabled
        submitEnable = true
        for n in 0...8 {
            if answerState[n] == [false,false,false,false,false] {
                submitEnable = false
            }
        }
        if ageAnswered == false {
            submitEnable = false
        }
        let selectedIndexPath = IndexPath(item:11 , section: 0)
        self.tableView.reloadRows(at: [selectedIndexPath], with: .none)
    }
    
    @objc func twoSelected(_ sender: UIButton){
        answerState.updateValue([false,false,true,false,false], forKey: sender.tag)
        // Check if submit button needs to be enabled
        submitEnable = true
        for n in 0...8 {
            if answerState[n] == [false,false,false,false,false] {
                submitEnable = false
            }
        }
        if ageAnswered == false {
            submitEnable = false
        }
        let selectedIndexPath = IndexPath(item:11 , section: 0)
        self.tableView.reloadRows(at: [selectedIndexPath], with: .none)
    }
    
    @objc func threeSelected(_ sender: UIButton){
        answerState.updateValue([false,false,false,true,false], forKey: sender.tag)
        // Check if submit button needs to be enabled
        submitEnable = true
        for n in 0...8 {
            if answerState[n] == [false,false,false,false,false] {
                submitEnable = false
            }
        }
        if ageAnswered == false {
            submitEnable = false
        }
        let selectedIndexPath = IndexPath(item:11 , section: 0)
        self.tableView.reloadRows(at: [selectedIndexPath], with: .none)
    }
    
    @objc func fourSelected(_ sender: UIButton){
        answerState.updateValue([false,false,false,false,true], forKey: sender.tag)
        // Check if submit button needs to be enabled
        submitEnable = true
        for n in 0...8 {
            if answerState[n] == [false,false,false,false,false] {
                submitEnable = false
            }
        }
        if ageAnswered == false {
            submitEnable = false
        }
        let selectedIndexPath = IndexPath(item:11 , section: 0)
        self.tableView.reloadRows(at: [selectedIndexPath], with: .none)
    }
    
    @objc func textFieldsIsNotEmpty(sender: UITextField) {
        if sender.text != "" {
            ageAnswered = true
        } else {
            ageAnswered = false
        }
        submitEnable = true
        for n in 0...8 {
            if answerState[n] == [false,false,false,false,false] {
                submitEnable = false
            }
        }
        if ageAnswered == false {
            submitEnable = false
        }
        let selectedIndexPath = IndexPath(item:11 , section: 0)
        self.tableView.reloadRows(at: [selectedIndexPath], with: .none)
    }
    
    private func loadImage() {
        // Add all question image to the list
        list.append(UIImage(named: "hair1")!)
        list.append(UIImage(named: "hair2")!)
        list.append(UIImage(named: "hair3")!)
        list.append(UIImage(named: "hair4")!)
        list.append(UIImage(named: "hair5")!)
        list.append(UIImage(named: "hair6")!)
        list.append(UIImage(named: "hair7")!)
        list.append(UIImage(named: "hair8")!)
        list.append(UIImage(named: "hair9")!)
        
        // Initialize all radio button states
        for n in 0...8 {
            answerState.updateValue([false,false,false,false,false], forKey: n)
        }
    }
}
