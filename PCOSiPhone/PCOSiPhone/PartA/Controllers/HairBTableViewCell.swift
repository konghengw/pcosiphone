//
//  HairBTableViewCell.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/5/4.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit

class HairBTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UITextField!
    @IBOutlet weak var question: UITextField!
    @IBOutlet weak var answerDescr: UITextField!
    @IBOutlet weak var answerField: UITextField!
    @IBOutlet weak var nextQuestion: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
