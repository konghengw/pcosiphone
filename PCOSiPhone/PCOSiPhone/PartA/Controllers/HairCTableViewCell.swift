//
//  HairCTableViewCell.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/5/4.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit

class HairCTableViewCell: UITableViewCell {

    @IBOutlet weak var optionZero: UITextField!
    @IBOutlet weak var optionOne: UITextField!
    @IBOutlet weak var optionTwo: UITextField!
    @IBOutlet weak var optionThree: UITextField!
    @IBOutlet weak var optionFour: UITextField!
    @IBOutlet weak var optionText: UITextView!
    @IBOutlet weak var hairImage: UIImageView!
    @IBOutlet weak var buttonZero: DLRadioButton!
    @IBOutlet weak var buttonOne: DLRadioButton!
    @IBOutlet weak var buttonTwo: DLRadioButton!
    @IBOutlet weak var buttonThree: DLRadioButton!
    @IBOutlet weak var buttonFour: DLRadioButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
