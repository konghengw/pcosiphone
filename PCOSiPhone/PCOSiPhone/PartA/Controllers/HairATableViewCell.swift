//
//  HairATableViewCell.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/5/4.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit

class HairATableViewCell: UITableViewCell {

    @IBOutlet weak var title: UITextField!
    @IBOutlet weak var baseQuestion: UITextView!
    @IBOutlet weak var baseYes: UITextField!
    @IBOutlet weak var baseNo: UITextField!
    @IBOutlet weak var yesButton: DLRadioButton!
    @IBOutlet weak var noButton: DLRadioButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
