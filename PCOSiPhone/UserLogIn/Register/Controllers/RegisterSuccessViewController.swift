//
//  RegisterSuccessViewController.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/5/11.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit

class RegisterSuccessViewController: UIViewController {

    @IBOutlet weak var continueButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        continueButton.layer.cornerRadius = 4
    }

}
