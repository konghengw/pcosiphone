//
//  RegisterViewController.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/5/11.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit
import Alamofire

class RegisterViewController: UIViewController {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var register: UIButton!
    @IBOutlet weak var cancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Disable register button
        register.layer.cornerRadius = 4
        register.isEnabled = false
        register.alpha = 0.5
        cancel.layer.cornerRadius = 4
        
        setupAddTargetIsNotEmptyTextFields()
    }
    
    func setupAddTargetIsNotEmptyTextFields() {
        username.addTarget(self, action: #selector(textFieldsIsNotEmpty),
                                for: .editingChanged)
        password.addTarget(self, action: #selector(textFieldsIsNotEmpty),
                                for: .editingChanged)
    }
    
    @objc func textFieldsIsNotEmpty(sender: UITextField) {
        // Enable register button if text field not empty
        if username?.text != "" && password?.text != "" {
            register.isEnabled = true
            register.alpha = 1
        } else {
            register.isEnabled = false
            register.alpha = 0.5
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.performSegue(withIdentifier: "hasaccountgotologin", sender: self)
    }
    
    @IBAction func register(_ sender: Any) {
        let url = URL(string: "http://localhost/register.php")!
        
        let parameters: [String: String] = [
            "username": username.text!,
            "password": password.text!,
            ]
        
        // Send registration request to the API
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
            if let res = response.result.value {
                if let resStatus = ((res as AnyObject)["status"]) {
                    if resStatus as! String == "error" {
                        if let resLog = ((res as AnyObject)["message"]) {
                            if resLog as! String == "User already exists" {
                                let s: String = self.username.text!
                                let alert = UIAlertController(title: "Log in as \(s)?", message: "It looks like you already have an account", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("Login as \(s)", comment: "Default action"), style: .default, handler: { _ in
                                    self.performSegue(withIdentifier: "hasaccountgotologin", sender: self)
                                }))
                                alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "dismiss action"), style: .default, handler: { _ in
                                    self.username.text = ""
                                    self.password.text = ""
                                    self.register.isEnabled = false
                                    self.register.alpha = 0.5
                                }))
                                self.present(alert, animated: true, completion: nil)
                            } else {
                                NSLog("There is something wrong with log from sign up request")
                                print(resLog as! String)
                            }
                        }
                        
                    } else if resStatus as! String == "Success" {
                        UserDefaults.standard.set(self.username.text!, forKey: "username")
                        self.performSegue(withIdentifier: "registerSuccess", sender: self)
                    }
                }
            }
        }
    }
}
