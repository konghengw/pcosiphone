//
//  LogInViewController.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/5/5.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit
import Alamofire

class LogInViewController: UIViewController {

    @IBOutlet weak var patientIDTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    var isUserLogIn:Bool = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        logInButton.layer.cornerRadius = 4
        logInButton.isEnabled = false
        logInButton.alpha = 0.5
        
        setupAddTargetIsNotEmptyTextFields()
    }

    func setupAddTargetIsNotEmptyTextFields() {
        patientIDTextField.addTarget(self, action: #selector(textFieldsIsNotEmpty),
                                for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldsIsNotEmpty),
                                for: .editingChanged)
    }
    
    @objc func textFieldsIsNotEmpty(sender: UITextField) {
        if patientIDTextField?.text != "" && passwordTextField?.text != "" {
            logInButton.isEnabled = true
            logInButton.alpha = 1
        } else {
            logInButton.isEnabled = false
            logInButton.alpha = 0.5
        }
    }
    
    @IBAction func LogIn(_ sender: Any) {
        let url = URL(string: "http://localhost/logIn.php")!
        
        let parameters: [String: String] = [
            "patient_id": patientIDTextField.text!,
            "password": passwordTextField.text!
        ]
        
        // Send log in request to API
        Alamofire.request(url, method: .post, parameters: parameters).responseJSON { response in
            if let res = response.result.value {
                if let resStatus = ((res as AnyObject)["status"]) {
                    if resStatus as! String == "error" {
                                let alert = UIAlertController(title: "Log In Error", message: "The patient_id or password is incorrect. Please check and try again", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("Try Again", comment: "Default action"), style: .default, handler: { _ in
                                    NSLog("The \"Incorrect Username\" alert occured.")
                                }))
                                self.present(alert, animated: true, completion: nil)
                    } else if resStatus as! String == "Success" {
                        UserDefaults.standard.set(self.patientIDTextField.text!, forKey: "username")
                        self.performSegue(withIdentifier: "EnterAccount", sender: self)
                    } else {
                        NSLog("There is an error getting status from login in request")
                        print(resStatus as! String)
                    }
                }
            }
        }
    }

}
