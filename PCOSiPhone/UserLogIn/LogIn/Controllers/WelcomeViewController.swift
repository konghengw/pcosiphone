//
//  WelcomeViewController.swift
//  PCOSiPhone
//
//  Created by macbook pro on 2019/5/5.
//  Copyright © 2019年 macbook pro. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var toPartA: UIButton!
    @IBOutlet weak var toPartB: UIButton!
    @IBOutlet weak var toSummary: UIButton!
    @IBOutlet weak var logOut: UIButton!
    @IBOutlet weak var userLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        toPartA.layer.cornerRadius = 4
        //toPartA.isEnabled = false
        //toPartA.alpha = 0.5
        
        toPartB.layer.cornerRadius = 4
        //toPartB.isEnabled = false
        //toPartB.alpha = 0.5
        
        toSummary.layer.cornerRadius = 4
        //toSummary.isEnabled = false
        //toSummary.alpha = 0.5
        
        logOut.layer.cornerRadius = 4
        
        userLabel.text = UserDefaults.standard.string(forKey: "username")  ?? ""
        userLabel.font = UIFont(name:"American Typewriter", size: 20.0)
        userLabel.textAlignment = .center
    }

}
